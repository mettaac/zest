package com.example.android.common.logger;

public class MessageOnlyLogFilter implements LogNode {

    LogNode zNext;

    public MessageOnlyLogFilter(LogNode next) {
        zNext = next;
    }

    public MessageOnlyLogFilter() {
    }

    @Override
    public void println(int priority, String tag, String msg, Throwable tr) {
        if (zNext != null) {
            getNext().println(Log.NONE, null, msg, null);
        }
    }

    public LogNode getNext() {
        return zNext;
    }

     public void setNext(LogNode node) {
        zNext = node;
    }

}
