package com.example.android.common.logger;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;

public class LogFragment extends Fragment {

    private LogView zLogView;
    private ScrollView zScrollView;

    public LogFragment() {}

    public View inflateViews() {
        zScrollView = new ScrollView(getActivity());
        ViewGroup.LayoutParams scrollParams = new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        zScrollView.setLayoutParams(scrollParams);

        zLogView = new LogView(getActivity());
        ViewGroup.LayoutParams logParams = new ViewGroup.LayoutParams(scrollParams);
        logParams.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        zLogView.setLayoutParams(logParams);
        zLogView.setClickable(true);
        zLogView.setFocusable(true);
        zLogView.setTypeface(Typeface.MONOSPACE);

        // Want to set padding as 16 dips, setPadding takes pixels.  Hooray math!
        int paddingDips = 16;
        double scale = getResources().getDisplayMetrics().density;
        int paddingPixels = (int) ((paddingDips * (scale)) + .5);
        zLogView.setPadding(paddingPixels, paddingPixels, paddingPixels, paddingPixels);
        zLogView.setCompoundDrawablePadding(paddingPixels);

        zLogView.setGravity(Gravity.BOTTOM);
        zLogView.setTextAppearance(getActivity(), android.R.style.TextAppearance_Holo_Medium);

        zScrollView.addView(zLogView);
        return zScrollView;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View result = inflateViews();

        zLogView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                zScrollView.fullScroll(ScrollView.FOCUS_DOWN);
            }
        });
        return result;
    }

    public LogView getLogView() {
        return zLogView;
    }
}