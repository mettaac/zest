package com.example.android.wearable.recipeassistant;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONObject;

public class RecipeActivity extends Activity {
    private static final String TAG = "ZEST";
    private String namaResep;
    private Recipe zResep;
    private ImageView zFoto;
    private TextView zTV; // text view
    private TextView zdescTV; // description text view
    private TextView zIngTV; // ingredients text view
    private LinearLayout zSteps;

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = getIntent();
        namaResep = intent.getStringExtra(Constants.NAME_TO_LOAD);

        if (Log.isLoggable(TAG, Log.DEBUG)) {
            Log.d(TAG, "Intent: " + intent.toString() + " " + namaResep);
        }
        loadRecipe();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recipe);
        zTV = (TextView) findViewById(R.id.recipeTextTitle);
        zdescTV = (TextView) findViewById(R.id.recipeTextSummary);
        zFoto = (ImageView) findViewById(R.id.recipeImageView);
        zIngTV = (TextView) findViewById(R.id.textIngredients);
        zSteps = (LinearLayout) findViewById(R.id.layoutSteps);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.cook:
                startCooking();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void loadRecipe() {
        JSONObject jsonObject = AssetUtils.loadJSONAsset(this, namaResep);
        if (jsonObject != null) {
            zResep = Recipe.fromJson(this, jsonObject);
            if (zResep != null) {
                displayRecipe(zResep);
            }
        }
    }

    private void displayRecipe(Recipe recipe) {
        Animation fadeIn = AnimationUtils.loadAnimation(this, android.R.anim.fade_in);
        zTV.setAnimation(fadeIn);
        zTV.setText(recipe.judul);
        zdescTV.setText(recipe.deskripsi);
        if (recipe.foodpic != null) {
            zFoto.setAnimation(fadeIn);
            Bitmap recipeImage = AssetUtils.loadBitmapAsset(this, recipe.foodpic);
            zFoto.setImageBitmap(recipeImage);
        }
        zIngTV.setText(recipe.bahan);

        findViewById(R.id.ingredientsHeader).setAnimation(fadeIn);
        findViewById(R.id.ingredientsHeader).setVisibility(View.VISIBLE);
        findViewById(R.id.stepsHeader).setAnimation(fadeIn);

        findViewById(R.id.stepsHeader).setVisibility(View.VISIBLE);

        LayoutInflater inf = LayoutInflater.from(this);
        zSteps.removeAllViews();
        int stepNumber = 1;
        for (Recipe.RecipeStep step : recipe.recipeSteps) {
            View view = inf.inflate(R.layout.step_item, null);
            ((TextView) view.findViewById(R.id.textStep)).setText(
                    (stepNumber++) + ". " + step.stepText);
            zSteps.addView(view);
        }
    }

    private void startCooking() {
        Intent intent = new Intent(this, RecipeService.class);
        intent.setAction(Constants.ACTION_START_COOKING);
        intent.putExtra(Constants.EXTRA_RECIPE, zResep.toBundle());
        startService(intent);
    }
}
