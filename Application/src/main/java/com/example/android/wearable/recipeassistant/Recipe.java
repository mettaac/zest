package com.example.android.wearable.recipeassistant;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Recipe {
    private static final String TAG = "ZEST";

    public String judul;
    public String deskripsi;
    public String foodpic;
    public String bahan;

    public static class RecipeStep {
        RecipeStep() { }
        public String stepText;

        public Bundle toBundle() {
            Bundle bundle = new Bundle();
            bundle.putString(Constants.FIELD_STEP_TEXT, stepText);
            return bundle;
        }

        public static RecipeStep fromBundle(Bundle bundle) {
            RecipeStep recipeStep = new RecipeStep();
            recipeStep.stepText = bundle.getString(Constants.FIELD_STEP_TEXT);
            return recipeStep;
        }
    }
    ArrayList<RecipeStep> recipeSteps;

    public Recipe() {
        recipeSteps = new ArrayList<RecipeStep>();
    }

    public static Recipe fromJson(Context context, JSONObject json) {
        Recipe recipe = new Recipe();
        try {
            recipe.judul = json.getString(Constants.FIELD_TITLE);
            recipe.deskripsi = json.getString(Constants.FIELD_SUMMARY);
            if (json.has(Constants.FIELD_IMAGE)) {
                recipe.foodpic = json.getString(Constants.FIELD_IMAGE);
            }
            JSONArray ingredients = json.getJSONArray(Constants.FIELD_INGREDIENTS);
            recipe.bahan = "";
            for (int i = 0; i < ingredients.length(); i++) {
                recipe.bahan += " - "
                        + ingredients.getJSONObject(i).getString(Constants.FIELD_TEXT) + "\n";
            }

            JSONArray steps = json.getJSONArray(Constants.FIELD_STEPS);
            for (int i = 0; i < steps.length(); i++) {
                JSONObject step = steps.getJSONObject(i);
                RecipeStep recipeStep = new RecipeStep();
                recipeStep.stepText = step.getString(Constants.FIELD_TEXT);
                recipe.recipeSteps.add(recipeStep);
            }
        } catch (JSONException e) {
            Log.e(TAG, "Error Loading Recipe: " + e);
            return null;
        }
        return recipe;
    }

    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        bundle.putString(Constants.FIELD_TITLE, judul);
        bundle.putString(Constants.FIELD_SUMMARY, deskripsi);
        bundle.putString(Constants.FIELD_IMAGE, foodpic);
        bundle.putString(Constants.FIELD_INGREDIENTS, bahan);
        if (recipeSteps != null) {
            ArrayList<Parcelable> stepBundles = new ArrayList<Parcelable>(recipeSteps.size());
            for (RecipeStep recipeStep : recipeSteps) {
                stepBundles.add(recipeStep.toBundle());
            }
            bundle.putParcelableArrayList(Constants.FIELD_STEPS, stepBundles);
        }
        return bundle;
    }

    public static Recipe fromBundle(Bundle bundle) {
        Recipe recipe = new Recipe();
        recipe.judul = bundle.getString(Constants.FIELD_TITLE);
        recipe.deskripsi = bundle.getString(Constants.FIELD_SUMMARY);
        recipe.foodpic = bundle.getString(Constants.FIELD_IMAGE);
        recipe.bahan = bundle.getString(Constants.FIELD_INGREDIENTS);
        ArrayList<Parcelable> stepBundles =
                bundle.getParcelableArrayList(Constants.FIELD_STEPS);
        if (stepBundles != null) {
            for (Parcelable stepBundle : stepBundles) {
                recipe.recipeSteps.add(RecipeStep.fromBundle((Bundle) stepBundle));
            }
        }
        return recipe;
    }
}
