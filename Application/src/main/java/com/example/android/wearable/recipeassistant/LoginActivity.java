package com.example.android.wearable.recipeassistant;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class LoginActivity extends Activity
{
    EditText username, password;
    Button login;
    TextView registerconnector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
        login = (Button) findViewById(R.id.login);
        registerconnector = (TextView) findViewById(R.id.registerconnector);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (login(username.getText().toString(), password.getText().toString()) == 1) {
                    Toast.makeText(LoginActivity.this, "Successfully Login!", Toast.LENGTH_SHORT).show();
                    //open main page
                    startActivity(new Intent(getApplication(), MainActivity.class));
                } else {
                    Toast.makeText(LoginActivity.this, "Username or Password Incorrect! Please Try Again!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        registerconnector.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent registerIntent = new Intent(LoginActivity.this, SignupActivity.class);
                LoginActivity.this.startActivity(registerIntent);
            }

        });

    }

    // LOGIN
    private int login(String name, String pwd) {
        if (name.isEmpty() || pwd.isEmpty()) {
            return 0;
        } else if (name.equals("admin") && pwd.equals("1234")) {
            return 1;
        }

        return 0;
    }
}
