package com.example.android.wearable.recipeassistant;

import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class RecipeAdapter implements ListAdapter
{
    private String TAG = "ZEST";

    private class Item {
        String title;
        String name;
        String summary;
        Bitmap image;
    }

    private List<Item> zItems = new ArrayList<Item>();
    private Context zContext;
    private DataSetObserver zObserver;

    public RecipeAdapter(Context context) {
        zContext = context;
        loadRecipeList();
    }

    private void loadRecipeList() {
        JSONObject jsonObject = AssetUtils.loadJSONAsset(zContext, Constants.LIST_FILE);
        if (jsonObject != null) {
            List<Item> items = parseJson(jsonObject);
            appendItemsToList(items);
        }
    }

    private List<Item> parseJson(JSONObject json) {
        List<Item> result = new ArrayList<Item>();
        try {
            JSONArray items = json.getJSONArray(Constants.FIELD_LIST);
            Log.d(TAG, "" + items.length());
            for (int i = 0; i < items.length(); i++) {
                JSONObject item = items.getJSONObject(i);
                Item parsed = new Item();
                parsed.name = item.getString(Constants.FIELD_NAME);
                parsed.title = item.getString(Constants.FIELD_TITLE);
                if (item.has(Constants.FIELD_IMAGE)) {
                    String imageFile = item.getString(Constants.FIELD_IMAGE);
                    parsed.image = AssetUtils.loadBitmapAsset(zContext, imageFile);
                }
                parsed.summary = item.getString(Constants.FIELD_SUMMARY);
                result.add(parsed);
            }
        } catch (JSONException e) {
            Log.e(TAG, "Failed to parse recipe list: " + e);
        }
        return result;
    }

    private void appendItemsToList(List<Item> items) {
        zItems.addAll(items);
        if (zObserver != null) {
            zObserver.onChanged();
        }
    }

    @Override
    public int getCount() {
        return zItems.size();
    }

    @Override
    public Object getItem(int position) {
        return zItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            LayoutInflater inf = LayoutInflater.from(zContext);
            view = inf.inflate(R.layout.list_item, null);
        }
        Item item = (Item) getItem(position);
        TextView titleView = (TextView) view.findViewById(R.id.textTitle);
        TextView summaryView = (TextView) view.findViewById(R.id.textSummary);
        ImageView iv = (ImageView) view.findViewById(R.id.imageView);

        titleView.setText(item.title);
        summaryView.setText(item.summary);
        if (item.image != null) {
            iv.setImageBitmap(item.image);
        } else {
            iv.setImageDrawable(zContext.getResources().getDrawable(R.drawable.ic_noimage));
        }
        return view;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isEmpty() {
        return zItems.isEmpty();
    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {
        zObserver = observer;
    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {
        zObserver = null;
    }

    @Override
    public boolean areAllItemsEnabled() {
        return true;
    }

    @Override
    public boolean isEnabled(int position) {
        return true;
    }

    public String getItemName(int position) {
        return zItems.get(position).name;
    }
}
