package com.example.android.wearable.recipeassistant;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends ListActivity {
    private static final String TAG = "ZEST";

    private RecipeAdapter zAdapter;
    EditText editText;
    Integer num = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(android.R.layout.list_content);

        zAdapter = new RecipeAdapter(this);
        setListAdapter(zAdapter);
    }

    @Override
    protected void onListItemClick(ListView lv, View v, int position, long id) {
        if (Log.isLoggable(TAG, Log.DEBUG)) {
            Log.d(TAG, "onListItemClick " + position);
        }
        String itemName = zAdapter.getItemName(position);
        Intent intent = new Intent(getApplicationContext(), RecipeActivity.class);
        intent.putExtra(Constants.NAME_TO_LOAD, itemName);
        startActivity(intent);
    }
}



